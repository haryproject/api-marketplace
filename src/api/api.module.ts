import { HttpModule, Module } from '@nestjs/common';
import { ApiService } from './api.service';
import { ApiController } from './api.controller';
import { ResponseHandlerModule } from '../utils/response-handler/response-handler.module';
import { mysqlDatabaseForFeature } from '../utils/database-provider/database-provider.service';
import { ServiceDocumentations } from './api.entity';

@Module({
  imports: [
    HttpModule,
    ResponseHandlerModule,
    mysqlDatabaseForFeature([
      ServiceDocumentations,
    ]),
  ],
  providers: [ApiService],
  controllers: [ApiController],
  exports: [ApiService],
})
export class ApiModule {
}
