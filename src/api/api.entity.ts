import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

export class Service {

  constructor(id, name, tags, path, hosts, protocols) {
    this.id = id;
    this.name = name;
    this.tags = tags;
    this.path = path;
    this.hosts = hosts;
    this.protocols = protocols;
  }

  id: string;
  name: string;
  tags: [];
  path: [];
  hosts: [];
  protocols: [];
  documentation: string;
  description: string;
}

@Entity({
  name: 'service_docs',
})
export class ServiceDocumentations {
  @PrimaryGeneratedColumn({})
  id: number;

  @Column()
  description: string;

  @Column({})
  serviceId: string;

  @Column()
  documentationUrl: string;


}