import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Service, ServiceDocumentations } from './api.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ResponseHandlerService } from '../utils/response-handler/response-handler.service';

@Injectable()
export class ApiService {
  private kongMasterUrl: string;
  private kongApiKey: string;
  private requestHeader: any;

  constructor(
    private configService: ConfigService,
    private httpService: HttpService,
    private responseHandlerService: ResponseHandlerService,
    @InjectRepository(ServiceDocumentations)
    private serviceDocs: Repository<ServiceDocumentations>,
  ) {
    this.kongMasterUrl = this.configService.get<string>('KONG_MASTER_URL');
    this.kongApiKey = this.configService.get<string>('KONG_MASTER_API');
    this.requestHeader = { headers: { 'X-CHACA-KEY': this.kongApiKey } };
  }

  async getKongOpenRoutes(tag: string): Promise<Service[]> {
    const services = [];
    const payload = await this.httpService.get(this.kongMasterUrl + 'routes', { params: { tags: [tag] }, ...this.requestHeader }).toPromise()
      .catch(error => {
        throw this.responseHandlerService.error('BAD_REQUEST', error.response.data);
      });
    const kongServices = payload.data ? payload.data.data : [];
    kongServices.map(service => {
      services.push(new Service(service.id, service.name, service.tags, service.path, service.hosts, service.protocols));
    });
    return services;
  }

  async getDocumentation(service_id: string): Promise<ServiceDocumentations> {
    const payload = await this.httpService.get(this.kongMasterUrl + 'routes/' + service_id, { ...this.requestHeader }).toPromise()
      .catch(error => {
        throw this.responseHandlerService.error('BAD_REQUEST', error.response.data);
      });

    let service = payload.data ? payload.data : [];
    service = new Service(service.id, service.name, service.tags, service.path, service.hosts, service.protocols);
    const documentation = await this.serviceDocs.findOne({ serviceId: service.id });
    service.documentation = documentation ? documentation.documentationUrl : '';
    service.description = documentation ? documentation.description : '';
    return service;
  }

  async createDocumentation(service_id: string, serviceDoc: ServiceDocumentations): Promise<ServiceDocumentations> {
    serviceDoc.serviceId = service_id;
    const documentation = await this.serviceDocs.save(serviceDoc).catch(error => {
      console.log(error);
      throw this.responseHandlerService.error('BAD_REQUEST', { message: 'Cannot Save documentation' });
    });
    return documentation;
  }
}
