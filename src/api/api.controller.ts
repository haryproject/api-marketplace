import { Controller, Get, HttpException, HttpStatus, Post, Query, Param, Body } from '@nestjs/common';
import { ApiService } from './api.service';
import { ResponseHandlerService } from '../utils/response-handler/response-handler.service';

@Controller('api')
export class ApiController {
  constructor(
    private apiService: ApiService,
    private responseHandler: ResponseHandlerService,
  ) {
  }

  @Get('/services')
  async getListedService(
    @Query() query,
  ) {
    let { tag } = query;
    tag = tag ? tag : 'openservice';

    const apiList = await this.apiService.getKongOpenRoutes(tag)
      .catch(error => {
        throw new HttpException(this.responseHandler.error('BAD_REQUEST', error), HttpStatus.BAD_REQUEST);
      });
    return apiList;
  }

  @Get('/docs/:service_id')
  async getServiceInformation(
    @Param() params,
  ) {
    const { service_id } = params;
    const serviceDetil = await this.apiService.getDocumentation(service_id)
      .catch(error => {
        throw new HttpException(this.responseHandler.error('BAD_REQUEST', error), HttpStatus.BAD_REQUEST);
      });
    return serviceDetil
  }

  @Post('/docs/:service_id')
  async storeServiceInformation(
    @Param() params,
    @Body() body,
  ) {
    const { service_id } = params;
    const serviceDetil = await this.apiService.createDocumentation(service_id,body)
      .catch(error => {
        throw new HttpException(error, HttpStatus.BAD_REQUEST);
      });
    return serviceDetil
  }
}
