import { Module } from '@nestjs/common';
import { DatabaseProviderService, databaseProviders } from './database-provider.service';

@Module({
  imports:[],
  providers: [DatabaseProviderService,...databaseProviders],
  exports:[DatabaseProviderService,...databaseProviders]
})
export class DatabaseProviderModule {}
