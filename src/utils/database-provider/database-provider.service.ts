import { Injectable, Inject } from '@nestjs/common';

import * as mongoose from 'mongoose';
import { ConfigService, ConfigModule } from '@nestjs/config';
import { SYS_CONFIG } from '../sys-config/configuration';
import * as fs from 'fs';
import { TypeOrmModule } from '@nestjs/typeorm';

export const databaseProviders = [
  {
    import: [ConfigModule],
    inject: [ConfigService],
    provide: SYS_CONFIG.DATABASE.MONGO_CONNECTION,
    useFactory: async (configService: ConfigService): Promise<typeof mongoose> => {
      const certificate = [fs.readFileSync('credentials/aws_document/rds-combined-ca-bundle.pem')];
      const host = configService.get<string>('DATABASE_HOST', 'localhost');
      const port = configService.get<string>('DATABASE_PORT', '27017');
      const username = configService.get<string>('DATABASE_USERNAME', 'root');
      const password = configService.get<string>('DATABASE_PASSWORD');
      const db = configService.get<string>('DATABASE_NAME');
      const admin = configService.get<string>('DATABASE_ADMIN');

      // const mongo = `mongodb://${username}:${password}@${host}:${port}/${db}?authSource=${admin}`;
      // console.log({ mongo, data });
      const mongoUrl = `mongodb://${username}:${password}@${host}:${port}/?ssl=true&replicaSet=rs0&readPreference=secondaryPreferred`;
      console.log({ mongoUrl });
      const connection = await mongoose.connect(mongoUrl, {
        sslValidate: true,
        sslCA: certificate,
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });

      return connection;
    },
  },
];


export const mysqlDatabaseForRoot = (entities) => {
  return TypeOrmModule.forRoot({
    type: 'mysql',
    host: 'cakepin.cnireqmbdrar.ap-southeast-1.rds.amazonaws.com',
    port: 3306,
    username: 'cakepin',
    password: 'cakep1234',
    database: 'api_marketplace',
    entities: entities,
    synchronize: true,
    autoLoadEntities: true,
  });
};

export const mysqlDatabaseForFeature = (entities) => {
  return TypeOrmModule.forFeature(entities);
};

@Injectable()
export class DatabaseProviderService {
}
