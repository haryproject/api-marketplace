import { extname } from "path";
import { HttpException, HttpStatus } from "@nestjs/common";
import { ResponseHandlerService } from "./response-handler/response-handler.service";
import { SYS_CONFIG } from './sys-config/configuration';

export const imageFileFilter = (req, file, callback) => {
  if (!file.originalname.match(/\.(jpg|jpeg|png|gif|JPG)$/)) {
    return callback(new HttpException(new ResponseHandlerService().error(SYS_CONFIG.ERROR.UPLOAD_PIC_INVALID, "Image with jpg|jpeg|png|gif|JPG are allowed"), HttpStatus.BAD_REQUEST), false);
  }
  callback(null, true);
};

export const editFileName = (req, file, callback, name) => {
  const fileExtName = extname(file.originalname);
  const randomName = Array(8)
    .fill(null)
    .map(() => Math.round(Math.random() * 16 * 1000).toString(16))
    .join('');
  callback(null, `${randomName}${fileExtName}`);
};


export const constructFileName = (file, flag: string) => {
  const fileExtName = extname(file.originalname);
  const randomName = Array(8)
    .fill(null)
    .map(() => Math.round(Math.random() * 16 * 1000).toString(16))
    .join('');
  return `${flag}${fileExtName}`;
};