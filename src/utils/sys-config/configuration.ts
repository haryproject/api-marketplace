export const SYS_CONFIG = {
    DATABASE: {
        MONGO_CONNECTION: "MONGO_CONNECTION"
    },
    ERROR: {
        UPLOAD_PIC_INVALID: "UPLOAD_PIC_INVALID",
        FAILED_UPLOAD_PIC: "FAILED_UPLOAD_PIC",
    }
}
