import { Injectable } from '@nestjs/common';
import { Response } from 'express';

@Injectable()
export class ResponseHandlerService {

  success(responsePayload: any): {responsePayload} {
    return responsePayload;
  }

  error(statusCode: string, errors: any): { statusCode: string, errors: any } {
    return { statusCode, errors };
  }
}