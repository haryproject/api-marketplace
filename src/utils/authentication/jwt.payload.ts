export class JwtPayload {
    iss: string
    user: Account
    iat: string
    exp: string
}

export class Account {
    id: string
    displayname: string
    fullname: string
    id_no: string
    dob: Date
    address: string
    id_type: string
    phone_number: string
    city: string
    state: string
    nation: string
    zip_code: string
    created_at: string
    updated_at: string
    account: AccessAccount[]
}


export class AccessAccount {
    uid: string
    value: string
    type: string
}