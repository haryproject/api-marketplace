import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { JwtPayloadEntity } from '../auth/strategy/jwt.payload';

export const User = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const user: JwtPayloadEntity = request.user;
    return user;
  },
);