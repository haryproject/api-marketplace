export const parseQueryParam = async function (query: any): Promise<any> {
    let queryStr = JSON.stringify(query)
    queryStr = queryStr.replace(/\b(eq|gt|gte|in|lt|lte|ne|nin)\b/g, match => `$${match}`);
    queryStr = JSON.parse(queryStr)
    return queryStr
}