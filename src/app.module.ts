import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import { ResponseHandlerModule } from './utils/response-handler/response-handler.module';
import { AccountModule } from './account/account.module';
import { ApiModule } from './api/api.module';
import { mysqlDatabaseForRoot } from './utils/database-provider/database-provider.service';
import { AccountEntity } from './account/account.entity';
import { ServiceDocumentations } from './api/api.entity';
import { UserGradeModule } from './user-grade/user-grade.module';
import { UserGrade } from './user-grade/user-grade.entity';

@Module({
  imports: [
    ResponseHandlerModule,
    AuthModule,
    mysqlDatabaseForRoot([
      AccountEntity,
      ServiceDocumentations,
      UserGrade
    ]),
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    AccountModule,
    ApiModule,
    UserGradeModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
}
