import { HttpException, HttpService, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  AccountEntity,
  FirebaseAccount, FirebaseIdTokenVerificationPayload,
  KongConsumer, KongConsumerAssignDailyRateLimit,
  KongConsumerAssignGroup, KongConsumerAssignJWTCredential,
  KongConsumerAssignKey,
} from './account.entity';

import * as admin from 'firebase-admin';
import { ResponseHandlerService } from '../utils/response-handler/response-handler.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import DecodedIdToken = admin.auth.DecodedIdToken;
import { METHODS } from 'http';

@Injectable()
export class AccountService {

  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
    private responseHandlerService: ResponseHandlerService,
    @InjectRepository(AccountEntity)
    private accountRepository: Repository<AccountEntity>,
  ) {

  }


  async createAccount(account: AccountEntity): Promise<AccountEntity> {
    const accountDB = await this.accountRepository.save(account).catch(error => {
      throw new HttpException(this.responseHandlerService.error('BAD_REQUEST', error), HttpStatus.BAD_REQUEST);
    });

    const createdAccountDB = await this.accountRepository.findOne({ id: accountDB.id });
    return createdAccountDB;
  }

  async findAccount(filter: any): Promise<AccountEntity> {
    const accountDB = await this.accountRepository.findOne(filter, {}).catch(error => {
      throw new HttpException(this.responseHandlerService.error('BAD_REQUEST', error), HttpStatus.BAD_REQUEST);
    });
    return accountDB;
  }

  async updateAccount(accountId: string, account: AccountEntity): Promise<AccountEntity> {
    await this.accountRepository.update(accountId, account).catch(error => {
      throw new HttpException(this.responseHandlerService.error('BAD_REQUEST', error), HttpStatus.BAD_REQUEST);
    });
    return await this.accountRepository.findOne(accountId);
  }


}
