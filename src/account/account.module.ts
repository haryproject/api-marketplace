import { HttpModule, Module } from '@nestjs/common';
import { AccountService } from './account.service';
import { AccountController } from './account.controller';
import { ResponseHandlerModule } from '../utils/response-handler/response-handler.module';
import { mysqlDatabaseForFeature } from '../utils/database-provider/database-provider.service';
import { AccountEntity } from './account.entity';
import { AccountKongService } from './account.kongservice';
import { AccountFirebaseService } from './account.firebaseservice';
import { UserGradeModule } from '../user-grade/user-grade.module';


@Module({
  imports: [
    HttpModule,
    ResponseHandlerModule,
    UserGradeModule,
    mysqlDatabaseForFeature([
      AccountEntity,
    ]),
  ],
  providers: [AccountService, AccountKongService, AccountFirebaseService],
  exports: [AccountService, AccountKongService, AccountFirebaseService],
  controllers: [AccountController],
})
export class AccountModule {
}
