import { HttpService, Injectable } from '@nestjs/common';
import {
  AccountEntity,
  KongConsumer,
  KongConsumerAssignDailyRateLimit,
  KongConsumerAssignGroup, KongConsumerAssignJWTCredential,
  KongConsumerAssignKey,
} from './account.entity';
import { ConfigService } from '@nestjs/config';
import { ResponseHandlerService } from '../utils/response-handler/response-handler.service';

@Injectable()
export class AccountKongService {
  private kongMasterUrl: string;
  private kongApiKey: string;
  private requestHeader: any;

  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
    private responseHandlerService: ResponseHandlerService,
  ) {
    this.kongMasterUrl = this.configService.get<string>('KONG_MASTER_URL');
    this.kongApiKey = this.configService.get<string>('KONG_MASTER_API');
    this.requestHeader = { headers: { 'X-CHACA-KEY': this.kongApiKey } };
  }


  async getConsumerFromKong(): Promise<[]> {
    const payload = await this.httpService.get(this.kongMasterUrl + 'consumers/', this.requestHeader).toPromise();
    const userList = payload.data.data.length ? payload.data.data : [];
    return userList;
  }

  async createConsumerForKong(uid: string, email: string): Promise<KongConsumer> {
    const payload = await this.httpService.post(this.kongMasterUrl + 'consumers', {
      custom_id: uid,
      username: email,
    }, this.requestHeader).toPromise().catch(error => {
      throw  error.response.data;
    });
    return payload.data;
  }

  async createAPIKeyForConsumer(consumer_id: string): Promise<KongConsumerAssignKey> {
    const kongUrl = this.kongMasterUrl + 'consumers/' + consumer_id + '/key-auth';
    const payload = await this.httpService.post(kongUrl, {}, this.requestHeader).toPromise().catch(error => {
      throw this.responseHandlerService.error('BAD_REQUEST', error.response.data);
    });
    return payload.data;
  }

  async createJWTKeyForConsumer(consumer_id: string): Promise<KongConsumerAssignJWTCredential> {
    const kongUrl = this.kongMasterUrl + 'consumers/' + consumer_id + '/jwt';
    const payload = await this.httpService.post(kongUrl, {}, this.requestHeader).toPromise().catch(error => {
      throw this.responseHandlerService.error('BAD_REQUEST', error.response.data);
    });
    return payload.data;
  }

  async updateGradeForUser(consumer_id: string, account: AccountEntity, grade: string, accessLimitPerDay: number): Promise<{ gradeId: string, dailyLimitId: string, grade: any[], limit: number }> {
    await this.deleteAclToCostumer(consumer_id, account.aclId).catch(error => {
      throw this.responseHandlerService.error('CANNOT_MODIFY_ACL', error);
    });
    const kongConsumerGroup = await this.createAclToConsumer(consumer_id, grade);
    const dailyLimit = await this.updateDailyRateLimitToUser(consumer_id, account.daliyLimitId, accessLimitPerDay);
    const { config } = dailyLimit;
    return {
      gradeId: kongConsumerGroup.id,
      grade: [kongConsumerGroup.group],
      limit: config.day,
      dailyLimitId: dailyLimit.id,
    };
  }

  async assignGradeForUser(consumer_id: string, grade: string, accessLimitPerDay: number): Promise<{ gradeId: string, dailyLimitId: string, grade: any[], limit: number }> {
    const kongConsumerGroup = await this.createAclToConsumer(consumer_id, grade);
    const dailyLimit = await this.createDailyRateLimitToUser(consumer_id, accessLimitPerDay);
    const { config } = dailyLimit;
    return {
      gradeId: kongConsumerGroup.id,
      grade: [kongConsumerGroup.group],
      limit: config.day,
      dailyLimitId: dailyLimit.id,
    };
  }

  private async createDailyRateLimitToUser(consumer_id: string, limit: number): Promise<KongConsumerAssignDailyRateLimit> {
    const kongUrl = this.kongMasterUrl + 'consumers/' + consumer_id + '/plugins';
    const payload = await this.httpService.post(kongUrl, {
      name: 'rate-limiting',
      config: {
        day: limit,
      },
    }, this.requestHeader).toPromise().catch(error => {
      throw this.responseHandlerService.error('BAD_REQUEST', error.response.data);
    });
    return payload.data;
  }

  private async updateDailyRateLimitToUser(consumer_id: string, dailyLimitId: string, limit: number): Promise<KongConsumerAssignDailyRateLimit> {
    const kongUrl = this.kongMasterUrl + 'consumers/' + consumer_id + '/plugins/' + dailyLimitId;

    const payload = await this.httpService.patch(kongUrl,
      {
        name: 'rate-limiting',
        config: {
          day: limit,
        },
      }, this.requestHeader).toPromise().catch(error => {
      throw this.responseHandlerService.error('BAD_REQUEST', error);
    });
    return payload.data;
  }

  private async createAclToConsumer(consumer_id: string, grade: string): Promise<KongConsumerAssignGroup> {
    const payload = await this.httpService.post(this.kongMasterUrl + 'consumers/' + consumer_id + '/acls', {
      group: grade,
    }, this.requestHeader).toPromise().catch(error => {
      throw  error.response.data;
    });
    return payload.data;
  }

  private async deleteAclToCostumer(consumer_id: string, pluginsId: string): Promise<KongConsumerAssignGroup> {
    const payload = await this.httpService.delete(this.kongMasterUrl + 'consumers/' + consumer_id + '/acls/' + pluginsId, this.requestHeader).toPromise().catch(error => {
      throw  error.response.data;
    });
    return payload.data;
  }

}