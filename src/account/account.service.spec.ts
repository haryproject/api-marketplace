import { Test, TestingModule } from '@nestjs/testing';
import { AccountService } from './account.service';
import { HttpModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

describe('AccountService', () => {
  let service: AccountService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule, HttpModule],
      providers: [AccountService],
    }).compile();

    service = module.get<AccountService>(AccountService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('Should return user list', async () => {
    const totalConsumer = await service.getConsumerFromKong();
    expect(totalConsumer.length).toBe(Number);
  });
});
