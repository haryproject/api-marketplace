import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  HttpException,
  HttpStatus,
  Post,
  Put,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { AccountService } from './account.service';
import { JwtAuthGuard } from '../auth/strategy/jwt.authguard';
import { User } from '../utils/custom-decorator';
import { JwtPayloadEntity } from '../auth/strategy/jwt.payload';
import { AccountFirebaseService } from './account.firebaseservice';
import { AccountKongService } from './account.kongservice';
import { UserGradeServices } from '../user-grade/user-grade.services';

@Controller('account')
@UseInterceptors(ClassSerializerInterceptor)
export class AccountController {
  constructor(
    private accountService: AccountService,
    private accountFirebaseService: AccountFirebaseService,
    private accountKongService: AccountKongService,
    private userGradeService: UserGradeServices,
  ) {
  }

  @Post('assign/apikey')
  async generateAPIKeyForUser(
    @Body() payload,
  ) {
    const { id } = payload;
    const account = await this.accountService.findAccount({ id }).catch(error => {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    });

    const apiKeyResult = await this.accountKongService.createAPIKeyForConsumer(account.kong_uid).catch(error => {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    });

    account.apiKey = apiKeyResult.key;
    const apiKeyPayload = await this.accountService.updateAccount(id, account).catch(error => {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    });

    return { key: apiKeyPayload.apiKey };
  }


  @Put('assign')
  @UseGuards(JwtAuthGuard)
  async assignToPro(
    @Body() payload,
    @User() jwtPayload: JwtPayloadEntity,
  ) {
    let { account } = jwtPayload;
    const { grade } = payload;

    account = await this.accountService.findAccount({ id: account.id });
    const userGrade = await this.userGradeService.findUserGrade({ id: grade });
    const proUser = await this.accountKongService.updateGradeForUser(account.kong_uid, account, userGrade.grade, userGrade.limit).catch(error => {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    });

    account.aclId = proUser.gradeId;
    account.daliyLimitId = proUser.dailyLimitId;
    account.dailyLimit = proUser.limit;
    account.grade = proUser.grade;

    const finalizedAccount = await this.accountService.updateAccount(account.id, account).catch(error => {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    });

    return finalizedAccount;
  }

}
