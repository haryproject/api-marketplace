import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import * as admin from 'firebase-admin';
import { FirebaseAccount } from './account.entity';
import DecodedIdToken = admin.auth.DecodedIdToken;

@Injectable()
export class AccountFirebaseService {
  private serviceAccount = 'credentials/firebase/api-marketplace-c7323-firebase-adminsdk-hvcks-d7aa8efcce.json';

  constructor() {
    admin.initializeApp({
      credential: admin.credential.cert(this.serviceAccount),
      databaseURL: 'https://api-marketplace-c7323.firebaseio.com/',
    });
  }

  async verifyJwtTokenFromFirebase(idToken: string): Promise<DecodedIdToken> {
    const user = await admin.auth().verifyIdToken(idToken).catch(error => {
      throw new HttpException({ message: error }, HttpStatus.BAD_REQUEST);
    });
    return user;
  }

  async generateJwtTokenFromFirebase(email: string, payload: any): Promise<{ token }> {
    const user = await admin.auth().getUserByEmail(email).catch(error => {
      throw new HttpException({ message: error }, HttpStatus.BAD_REQUEST);
    });

    const jwtToken = await admin.auth().createCustomToken(user.uid, { ...user, ...payload }).catch(error => {
      throw new HttpException({ message: error }, HttpStatus.BAD_REQUEST);
    });

    return { token: jwtToken };
  }

  async createFirebaseAccountByEmailAndPassword(email: string, password: string, displayName: string): Promise<FirebaseAccount> {
    const payload = await admin.auth().createUser({
      email: email,
      emailVerified: false,
      password: password,
      displayName: displayName,
      photoURL: 'http://www.example.com/12345678/photo.png',
      disabled: false,
    }).catch(error => {
      throw  error;
    });

    const account = new FirebaseAccount();
    account.displayname = payload.displayName;
    account.uid = payload.uid;
    account.photoUrl = payload.photoURL;
    account.email = payload.email;
    account.disabled = payload.disabled;
    account.emailVerified = payload.emailVerified;
    return account;
  }


}