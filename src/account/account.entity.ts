import { IsNotEmpty, IsUUID } from 'class-validator';
import { AfterLoad, BeforeInsert, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Exclude } from 'class-transformer';

export interface KongConsumer {
  custom_id: string
  id: string
  tags: []
  username: string
  created_at: number
}

export interface KongConsumerAssignKey {
  consumer: KongConsumer,
  created_at: number,
  id: string,
  key: string
}

export interface FirebaseIdTokenVerificationPayload {
  'name': string,
  'picture': string,
  'iss': string,
  'aud': string,
  'auth_time': number,
  'user_id': string,
  'sub': string,
  'iat': number,
  'exp': number,
  'email': string,
  'email_verified': boolean,
  'firebase': {
    'identities': {
      'google.com': string [],
      'email': string[]
    },
    'sign_in_provider': string
  },
  'uid': string
}

export interface KongConsumerAssignJWTCredential {
  'consumer_id': string,
  'created_at': number,
  'id': string,
  'key': string,
  'secret': string
}

export interface KongConsumerAssignDailyRateLimit {
  created_at: number,
  config: {
    minute: number,
    policy: string,
    month: number,
    redis_timeout: number,
    limit_by: string,
    hide_client_headers: boolean,
    second: number,
    day: number,
    redis_password: string,
    year: number,
    redis_database: string,
    hour: number,
    redis_port: number,
    redis_host: string,
    fault_tolerant: boolean
  },
  id: string,
  service: any,
  enabled: boolean,
  protocols: [],
  name: string,
  consumer: { id: string },
  route: any,
  tags: any


}

export interface KongConsumerAssignGroup {
  created_at: string,
  consumer: KongConsumer,
  id: string,
  group: string,
  tags: []
}

export class FirebaseAccount {
  uid: string;
  displayname: string;
  photoUrl: string;
  email: string;
  emailVerified: boolean;
  disabled: boolean;
}

@Entity({
  name: 'account',
})
export class AccountEntity {
  @PrimaryGeneratedColumn({})
  id: string;

  @Column()
  email: string;

  @Column()
  firebase_uid: string;

  @Column()
  kong_uid: string;

  @Column()
  displayName: string;

  @Column()
  apiKey: string;

  @Column({
    type: String,
  })
  grade: any;

  @Column()
  dailyLimit: number;

  @Column()
  @Exclude()
  daliyLimitId: string;

  @Column()
  @Exclude()
  aclId: string;

  @Column()
  @Exclude()
  jwtSecret: string;

  @Column()
  @Exclude()
  jwtIssuer: string;

  @BeforeInsert()
  private convertGradeArrayToString() {
    this.grade = this.grade.join(',');
  }

  @AfterLoad()
  private convertGradeStringToArray() {
    this.grade = this.grade.toString().split(',');
  }
}

export class AccountEntityDTO {
  @IsNotEmpty()
  email: string;
  @IsNotEmpty()
  password: string;
  @IsNotEmpty()
  displayName: string;
}