import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  Request,
  HttpException,
  HttpStatus,
  Inject,
  Post, UseGuards, UseInterceptors, SetMetadata,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { AccountService } from '../account/account.service';
import { ResponseHandlerService } from '../utils/response-handler/response-handler.service';
import { AccountEntity, AccountEntityDTO } from '../account/account.entity';
import { STATUS_CODES } from 'http';
import { JwtService } from '@nestjs/jwt';
import { JwtAuthGuard } from './strategy/jwt.authguard';
import { Grades } from './strategy/roles.decorator';
import { AccountFirebaseService } from '../account/account.firebaseservice';
import { AccountKongService } from '../account/account.kongservice';
import { VerifyEmailPassword, VerifyFirebaseIDTokenDTO } from './auth.dto';

@Controller('auth')
@UseInterceptors(ClassSerializerInterceptor)
export class AuthController {

  constructor(
    private authService: AuthService,
    private accountService: AccountService,
    private responseHandler: ResponseHandlerService,
    private accountFirebaseService: AccountFirebaseService,
    private accountKongService: AccountKongService,
    private jwtService: JwtService,
    private responseHandlerService: ResponseHandlerService,
  ) {
  }

  @Get('/account')
  @UseGuards(JwtAuthGuard)
  @Grades('basic', 'pro')
  async getAccount(
    @Request() req,
  ) {
    const user = req.user;
    return user;
  }

  @Post('register/emailpassword')
  async registerEmailPassword(
    @Body() payload: AccountEntityDTO,
  ) {
    const { email, password, displayName } = payload;
    const firebaseUser = await this.accountFirebaseService.createFirebaseAccountByEmailAndPassword(email, password, displayName).catch(error => {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    });

    const kongUser = await this.accountKongService.createConsumerForKong(firebaseUser.uid, firebaseUser.email).catch(error => {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    });

    const kongJwtCredential = await this.accountKongService.createJWTKeyForConsumer(kongUser.id).catch(error => {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    });

    const assignToBasicUser = await this.accountKongService.assignGradeForUser(kongUser.id, 'basic', 1000);


    const account = new AccountEntity();
    account.grade = assignToBasicUser.grade;
    account.email = email;
    account.displayName = displayName;
    account.firebase_uid = firebaseUser.uid;
    account.kong_uid = kongUser.id;
    account.jwtSecret = kongJwtCredential.secret;
    account.jwtIssuer = kongJwtCredential.key;
    account.dailyLimit = assignToBasicUser.limit;
    account.daliyLimitId = assignToBasicUser.dailyLimitId;
    account.aclId = assignToBasicUser.gradeId;

    const accountEntity = await this.accountService.createAccount(account).catch(error => {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    });

    return accountEntity;
  }

  @Post('retrieve/firebase/token')
  async retrieveJwtToken(
    @Body() payload: AccountEntityDTO,
  ) {
    const { email, password, displayName } = payload;
    const result = await this.accountFirebaseService.generateJwtTokenFromFirebase(email, {})
      .catch(error => {
        throw new HttpException(error, HttpStatus.BAD_REQUEST);
      });
    return result;
  }

  @Post('verify/firebase/token')
  async verifyFirebaseToken(
    @Body() payload: VerifyFirebaseIDTokenDTO,
  ) {
    const { idToken } = payload;
    const result = await this.accountFirebaseService.verifyJwtTokenFromFirebase(idToken);
    const account = await this.accountService.findAccount({ firebase_uid: result.uid });
    const jwt = await this.authService.generateJwtToken(account);
    return jwt;
  }


  @Post('generate/jwt')
  async generateJwtToken(
    @Body() payload,
  ) {
    const { id } = payload;
    const account = await this.accountService.findAccount({ id: id });

    const jwt = await this.authService.generateJwtToken(account);
    return jwt;
  }

  @Post('forgot/password')
  async sendForgotPasswordLink(
    @Body() payload,
  ) {
    const { email, password, displayName } = payload;
    const forgotPasswordLink = await this.authService.sendForgotPasswordLink(email)
      .catch(error => {
        return error;
      });

    return forgotPasswordLink;
  }

}
