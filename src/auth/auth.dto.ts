import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class VerifyFirebaseIDTokenDTO {
  @IsNotEmpty()
  idToken: string;
}

export class VerifyEmailPassword {
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  password: string;
}