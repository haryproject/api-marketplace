import { HttpModule, Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { DatabaseProviderModule } from '../utils/database-provider/database-provider.module';
import { ResponseHandlerModule } from '../utils/response-handler/response-handler.module';
import { AccountModule } from '../account/account.module';
import { JwtModule } from '@nestjs/jwt';
import { CONFIG } from './auth.staticconfig';
import { JwtStrategy } from './strategy/jwt.strategy';
import { APP_GUARD } from '@nestjs/core';
import { RolesGuard } from './strategy/roles.guards';

@Module({
  imports: [
    ResponseHandlerModule,
    HttpModule,
    AccountModule,
    JwtModule.register({
      secret: CONFIG.secret,
      signOptions: { expiresIn: '3600s' },
    }),

  ],
  providers: [
    AuthService,
    JwtStrategy,
    {
      provide: APP_GUARD,
      useClass: RolesGuard,
    },

  ],
  exports: [AuthService],
  controllers: [AuthController],
})
export class AuthModule {
}
