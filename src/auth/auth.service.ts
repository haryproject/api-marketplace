import { HttpService, Inject, Injectable } from '@nestjs/common';
import * as admin from 'firebase-admin';
import { ConfigService } from '@nestjs/config';
import { AccountService } from '../account/account.service';
import { JwtService } from '@nestjs/jwt';
import { AccountEntity } from '../account/account.entity';

@Injectable()
export class AuthService {
  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
    private accountService: AccountService,
    private jwtService: JwtService,
  ) {
  }

  async generateJwtToken(account: AccountEntity): Promise<{ access_token }> {
    delete account.jwtIssuer;
    delete account.jwtSecret;
    delete account.kong_uid;
    delete account.firebase_uid;
    delete account.apiKey;
    delete account.daliyLimitId;
    delete account.aclId;

    const payload = { account };

    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async sendForgotPasswordLink(email: string): Promise<{ forgoPasswordLink }> {
    const actionCodeSettings = {
      // URL you want to redirect back to. The domain (www.example.com) for
      // this URL must be whitelisted in the Firebase Console.
      url: 'https://chaca.io/',
      // This must be true for email link sign-in.
      handleCodeInApp: true,
      iOS: {
        bundleId: 'com.cakeplabs.cakepin',
      },
      android: {
        packageName: 'com.cakeplabs.cakepin',
        installApp: true,
        minimumVersion: '12',
      },
      // FDL custom domain.
      dynamicLinkDomain: 'https://chaca.io',
    };
    const payload = await admin.auth().generatePasswordResetLink(email, actionCodeSettings);
    return { forgoPasswordLink: payload };
  }

}
