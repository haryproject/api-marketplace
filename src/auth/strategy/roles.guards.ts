import { Injectable, CanActivate, ExecutionContext, HttpException, HttpStatus } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { JwtService } from '@nestjs/jwt';
import { JwtPayloadEntity } from './jwt.payload';
import { ResponseHandlerService } from '../../utils/response-handler/response-handler.service';

@Injectable()
export class RolesGuard extends AuthGuard('jwt') implements CanActivate {
  constructor(
    private reflector: Reflector,
    private jwtService: JwtService,
    private responseHandlerService: ResponseHandlerService,
  ) {
    super();
  }

  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get<string[]>('grades', context.getHandler());
    if (!roles) {
      return true;
    }
    try {
      const request = context.switchToHttp().getRequest();
      const bearer = request.headers.authorization.replace('Bearer ', '');
      const user: JwtPayloadEntity = this.jwtService.verify(bearer);
      return this.enoughGrades(roles, user);
    } catch (error) {
      throw new HttpException({
        'statusCode': 401,
        'message': 'Unauthorized',
      }, HttpStatus.UNAUTHORIZED);
    }
  }


  private enoughGrades(expectedGrades: string[], activeUser: JwtPayloadEntity): boolean {
    for (let i = 0; i < expectedGrades.length; i++) {
      const grade = expectedGrades[i];

      const isValid = activeUser.account.grade.includes(grade);
      if (isValid === true) {
        return true;
      }
    }
    return false;
  }
}