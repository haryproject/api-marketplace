import { SetMetadata } from '@nestjs/common';

export const Grades = (...grades: string[]) => SetMetadata('grades', grades);