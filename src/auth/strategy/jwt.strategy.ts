import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { CONFIG } from '../auth.staticconfig';
import { JwtPayloadEntity } from './jwt.payload';


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: CONFIG.secret,
    });
  }

  async validate(payload: JwtPayloadEntity): Promise<JwtPayloadEntity> {
    return payload;
  }
}