import {
  Body,
  ClassSerializerInterceptor,
  Controller, Delete,
  Get,
  HttpException, HttpStatus, Param,
  Post, Put,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { UserGradeServices } from './user-grade.services';
import { JwtAuthGuard } from '../auth/strategy/jwt.authguard';
import { Grades } from '../auth/strategy/roles.decorator';
import { UserGradeDto } from './user-grade.dto';

@Controller('user-grade')
@UseInterceptors(ClassSerializerInterceptor)
export class UserGradeController {
  constructor(
    private userGradeService: UserGradeServices,
  ) {
  }

  @Get('/')
  @UseGuards(JwtAuthGuard)
  // @Grades('admin')
  async retrieveUserGrade(
    @Body() payload,
  ) {
    return await this.userGradeService.retrieveUserGrades(payload);
  }

  @Post('/')
  @UseGuards(JwtAuthGuard)
  // @Grades('admin')
  async createUserGrade(
    @Body() payload: UserGradeDto,
  ) {
    const userGrade = await this.userGradeService.createUserGrade(payload).catch(error => {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    });
    return userGrade;
  }

  @Put('/:userGradeId')
  @UseGuards(JwtAuthGuard)
  // @Grades('admin')
  async updateUserGrade(
    @Param() params,
    @Body() payload: UserGradeDto,
  ) {
    const { userGradeId } = params;
    const userGrade = await this.userGradeService.updateUserGrade(userGradeId,payload)
    return userGrade;
  }


  @Delete('/:userGradeId')
  @UseGuards(JwtAuthGuard)
  // @Grades('admin')
  async deleteUserGrade(
    @Param() params,
  ) {
    const { userGradeId } = params;
    const userGrade = await this.userGradeService.deleteUserGrade(userGradeId)
    return userGrade;
  }


}