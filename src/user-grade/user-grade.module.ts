import { Module } from '@nestjs/common';
import { mysqlDatabaseForFeature } from '../utils/database-provider/database-provider.service';
import { UserGrade } from './user-grade.entity';
import { UserGradeController } from './user-grade.controller';
import { UserGradeServices } from './user-grade.services';
import { ResponseHandlerModule } from '../utils/response-handler/response-handler.module';

@Module({
  imports: [
    mysqlDatabaseForFeature([
      UserGrade,
    ]),
    ResponseHandlerModule,
  ],
  controllers: [
    UserGradeController,
  ],
  providers: [
    UserGradeServices,
  ],
  exports:[
    UserGradeServices
  ]
})
export class UserGradeModule {
}
