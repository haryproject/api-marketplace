import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';


@Entity({
  name: 'user_grade',
})
export class UserGrade {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  grade: string;

  @Column()
  limit: number;
}
