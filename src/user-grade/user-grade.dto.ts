import { IsNotEmpty, IsNumber } from 'class-validator';

export class UserGradeDto {
  id: number;

  @IsNotEmpty()
  grade: string;

  @IsNotEmpty()
  @IsNumber()
  limit: number;
}