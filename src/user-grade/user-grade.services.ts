import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserGrade } from './user-grade.entity';
import { Repository } from 'typeorm';
import { ResponseHandlerService } from '../utils/response-handler/response-handler.service';


@Injectable()
export class UserGradeServices {

  constructor(
    @InjectRepository(UserGrade)
    private userGradeRepository: Repository<UserGrade>,
    private responseHandlerService: ResponseHandlerService,
  ) {
  }

  async retrieveUserGrades(criteria: any): Promise<UserGrade[]> {
    return await this.userGradeRepository.find(criteria);
  }

  async findUserGrade(criteria: any): Promise<UserGrade> {
    return await this.userGradeRepository.findOne(criteria);
  }

  async createUserGrade(userGrade: UserGrade): Promise<UserGrade> {
    await this.userGradeRepository.save(userGrade).catch(error => {
      throw new HttpException(this.responseHandlerService.error('FAILED_TO_SAVE_USERGRADE', {}), HttpStatus.BAD_REQUEST);
    });

    const userGradeRepository = await this.userGradeRepository.findOne({ id: userGrade.id });
    return userGradeRepository;
  }

  async updateUserGrade(userGradeId: number, userGrade: UserGrade): Promise<UserGrade> {
    await this.userGradeRepository.findOneOrFail({ id: userGradeId }).catch(error => {
      throw new HttpException(this.responseHandlerService.error('USERGRADE_NOT_FOUND', { message: 'User Grade not found' }), HttpStatus.NOT_FOUND);
    });
    await this.userGradeRepository.update({ id: userGradeId }, userGrade).catch(error => {
      throw new HttpException(this.responseHandlerService.error('FAILED_TO_UPDATE_USERGRADE', {}), HttpStatus.BAD_REQUEST);
    });

    return await this.userGradeRepository.findOne(userGradeId);
  }

  async deleteUserGrade(userGradeId: number): Promise<UserGrade> {
    const userGradeRepository = await this.userGradeRepository.findOneOrFail({ id: userGradeId }).catch(error => {
      throw new HttpException(this.responseHandlerService.error('USERGRADE_NOT_FOUND', { message: 'User Grade not found' }), HttpStatus.NOT_FOUND);
    });
    await this.userGradeRepository.delete({ id: userGradeId }).catch(error => {
      throw this.responseHandlerService.error('FAILED_TO_DELETE_USERGRADE', {});
    });
    return userGradeRepository;
  }
}